import './App.css'

import AddReport from "./pages/reports/add-report"

function App() {

  return (
    <>
      <AddReport/>
    </>
  )
}

export default App
