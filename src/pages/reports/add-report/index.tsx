import GeneralPageWrapper from "../../../components/GeneralPageWrapper";
import ReportFrom from "./partials/ReportFrom";

export default function AddReport() {
  return (
    <GeneralPageWrapper>
        <div className="py-10 mx-auto text-center rounded-lg">
            <ReportFrom/>
        </div>
    </GeneralPageWrapper>
  )
}
