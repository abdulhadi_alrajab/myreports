import { useState } from "react";
import { useForm } from "react-hook-form";

interface FormData {
    date: string;
    employeeName: string;
    projectName: string;
    taskName: string;
    startTime: string;
    endTime: string;
  }
export default function ReportFrom() {
    const [formData, setFormData] = useState<FormData[]>([]);
    const { register, handleSubmit } = useForm<FormData>();
    const onSubmit = (data: FormData) => {
        setFormData(prev => [...prev, data]);
    };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="max-w-md mx-auto">
    <input 
      {...register("date")}
      type="date"
      className="border p-2 w-full" 
    />
    <input
      {...register("employeeName")}
      className="border p-2 w-full"
      placeholder="Employee name" 
    />

    <input
      {...register("projectName")}
      className="border p-2 w-full" 
      placeholder="Project name"
    />

    <input
      {...register("taskName")}
      className="border p-2 w-full"
      placeholder="Task name"
    />
    <div className="flex">
      <input 
        {...register("startTime")}
        type="time"
        className="border p-2 w-full mr-2"
      />
    <input
    {...register("endTime")} 
    type="time"
    className="border p-2 w-full"
    />
    </div>

    <button
      type="submit"
      className="bg-blue-500 text-white p-2 w-full mt-2"
    >
      Submit
    </button>
  </form>
  )
}
